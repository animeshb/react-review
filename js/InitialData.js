var keygen = require("keygenerator");
keygen.length = 4;
module.exports = {
  // Load Mock Product Data Into localStorage
  init: function() {
    localStorage.clear();
    localStorage.setItem('review', JSON.stringify([
      {
        id: keygen.number(),
        title: 'Ardbeg 10-Year',
        image: '',
        content: 'This is an excellent whiskey from the island Islay. Very smokey, the peat is in great abundance!',
        userId: "1"
      }
    ]));
    localStorage.setItem('user', JSON.stringify([
    {
      id: keygen.number(),
      name: "Scott",
      email: "wsmacewan101@gmail.com",
      password: "password"
    }
    ]));
  }
};