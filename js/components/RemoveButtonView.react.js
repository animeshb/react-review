var React = require('react');
var Actions = require('../actions/Actions');
var ReviewStore = require('../stores/ReviewStore');

var RemoveButtonView = React.createClass({

	remove: function(e){
		e.preventDefault();
		var revid = this.props.reviewsId;
		Actions.remReviews(revid);
	},

	render: function(){
		return (
			
				<button className="btn btn-default" onClick={this.remove}>X</button>
		);
	}
});

module.exports = RemoveButtonView;