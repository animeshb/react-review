var React = require('react');
var ReviewFormView = require('./ReviewFormView.react');
var SidebarRightView = require('./SidebarRightView.react');
var ReviewStore = require('../stores/ReviewStore');
var ReviewApi = require('../utils/ReviewApi');
var Router = require('react-router');
var Actions = require('../actions/Actions');
var Navigation = require('react-router').Navigation;
var authmixins = require('../mixins/authmixins');



function getState(revid){
	
	/*
	var reviews = ReviewStore.getAllReviews();
	if(reviews.length > 0){
		reviews = reviews.reverse();
	}
	*/
	return {
		//reviews: reviews,
		currentReview: ReviewStore.getReview(revid)
	}
}

var ViewReviewView = React.createClass({
	mixins: [Router.State,Navigation,authmixins],
	getInitialState: function(){
		//ReviewStore.reinitialize(this.getParams().reviewId);
		return getState(this.getParams().reviewId);
	},

	 // Add change listeners to stores
	componentDidMount: function() {
		ReviewStore.addChangeListener(this._onChange);
	},

	// Remove change listers from stores
	componentWillUnmount: function() {
		ReviewStore.removeChangeListener(this._onChange);
	},

	render: function () {

		var currentobj = this.state.currentReview;

		var reviewView;
			reviewView = (<div className="col-md-9">

				<form encType="multipart/form-data">
					<div className="form-group">
						<label htmlFor="review-title">Title</label>
						<input className="form-control" name="review-title" type="text" ref="reviewTitle" onChange={this.onChangeTitle} value={this.state.currentReview.title} />
					</div>
					<div className="form-group">
						<label htmlFor="review-content">Review</label>
						<textarea className="form-control" name="review-content" rows="20" ref="reviewContent" onChange={this.onChangeContent} value={this.state.currentReview.content}></textarea>
					</div>

					<div className="form-group">
						<label htmlFor="review-image">Image</label>
						<img src={this.state.currentReview.image} alt="boohoo"/>
						<input type="file" onChange={this.handleFile} />
					</div>

					 <button className="btn btn-default" onClick={this.submitForm}>Submit</button>
				</form>
				</div>);
		
		return (
			<div className="row">
				<h1>Edit a review</h1>
				{reviewView}
			</div>
		);
	},

	_onChange: function(){
		this.setState(getState());
	},

	handleFile: function(e) {
    var self = this;
    var reader = new FileReader();
    var file = e.target.files[0];

    var self = this;

   
	reader.onload = function(upload) {

	    var obj = self.state.currentReview;
		obj.image = upload.target.result;

		self.setState({currentReview: obj });
	      
	    }

	    reader.readAsDataURL(file);
	},

	onChangeContent: function(event) {

		var obj = this.state.currentReview;
		obj.content = event.target.value;
		this.setState({currentReview: obj });
		
	},

	onChangeTitle: function(event,self) {

		var obj = this.state.currentReview;
		obj.title = event.target.value;
		this.setState({currentReview: obj });


	},

	submitForm: function (e) {
		e.preventDefault();
		var obj = this.state.currentReview;
		Actions.editReviews(obj);
		this.transitionTo('reviewDashboard');
	}
});

module.exports = ViewReviewView;