var React = require('react');
var Actions = require('../actions/Actions');

var Navigation = require('react-router').Navigation;

var ReviewFormView = React.createClass({


	getInitialState: function() {
	    return {

	      
	      image: null

		};
	  },
	mixins: [Navigation],
	render: function () {
		var classString = (this.props.cssWidth) ? "col-md-" + this.props.cssWidth : "col-md-9";
		return (
			<div className={classString}>
				<form encType="multipart/form-data">
					<div className="form-group">
						<label htmlFor="review-title">Title</label>
						<input className="form-control" name="review-title" type="text" ref="reviewTitle"/>
					</div>
					<div className="form-group">
						<label htmlFor="review-content">Review</label>
						<textarea className="form-control" name="review-content" rows="20" ref="reviewContent"></textarea>
					</div>
					<div className="form-group">
						<label htmlFor="review-image">Image</label>
						 <input type="file" onChange={this.handleFile} />
					</div>
					 <button className="btn btn-default" onClick={this.submitForm}>Submit</button>
				</form>
			</div>
		);
	},

	handleFile: function(e) {
    var self = this;
    var reader = new FileReader();
    var file = e.target.files[0];

	    reader.onload = function(upload) {
	      self.setState({
	        image: upload.target.result,
	      });
	    }

	    reader.readAsDataURL(file);
	},

	submitForm: function (e) {
		e.preventDefault();
		var title = this.refs.reviewTitle.getDOMNode().value,
			content = this.refs.reviewContent.getDOMNode().value,
			image = this.state.image;

		var formData = {
			title: title,
			content: content,
			image:image
		};

		console.log( formData );

		Actions.postReview(formData);
		this.transitionTo('reviewDashboard');
	}
});

module.exports = ReviewFormView;