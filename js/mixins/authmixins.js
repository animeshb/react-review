var UserStore = require('../stores/UserStore');
var authmixins = {
  statics: {
    willTransitionTo: function (transition) {
      var nextPath = transition.path;
      if (!UserStore.isLoggedIn()) {
        transition.redirect('/',{}
          );
      }
    }
  }
};

module.exports = authmixins
