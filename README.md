My Comments 
===============
Demo Review App
Source : https://github.com/smacewan101/review-app

Aim was to get an overall idea abut React + Flux architecture.However, i have added following things 

1) Delete and redirect to review list.

2) Edit Review

3) Upload an Image using react.

4) Uauthorized access restriction of urls via ract router.



Original 
===============

About
-----
This is a demo app testing of the functionalites of react.js and flux.js together.
It is entirely front-end and runs off of local storage.



To Get Running
--------------
At the command line `npm start` and save a file and navigate to index.html in your browser, or run `npm build` and adjust the line in index.html to refer to `bundle.min.js`.